Summary
(Da el resumen del issue)

Steps to reproduce
(Indica los pasos para replicarlo)

What is the current behavior?
(Comportamiento actual)

What is the expected behavior?
(Comportamiento esperado)